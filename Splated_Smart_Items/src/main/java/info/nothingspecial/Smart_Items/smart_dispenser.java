package info.nothingspecial.Smart_Items;


import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Hopper;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Dispenser;
import org.bukkit.material.Wool;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.util.Arrays;
import java.util.List;

/* Smart_Items
 * Hopper Item frame filters
 * Falling block from dispensers
 * Arrows after Hitting object becomes Item stack again.
 * dispenser can use tools AXE, PICKAXE, SPADE, FISHING_ROD, HOE, SHEARS, SWORD
 */


public class smart_dispenser implements Listener {


    private Smart_Items plugin;

    public smart_dispenser(Smart_Items p) {
        this.plugin = p;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);


    }


    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryPickupItemEvent(InventoryPickupItemEvent event) {

        World world = event.getItem().getWorld();
        String[] Dissabled = plugin.getConfig().getString("DisabledWorlds").split(",");
        if (Arrays.asList(Dissabled).contains(world.getName())) return;

        if (event.getInventory().getHolder() instanceof Hopper) {
            Block hopperBlock = ((Hopper) event.getInventory().getHolder()).getBlock();

            if (!tools.PickItUp(hopperBlock, event.getItem()))
                event.setCancelled(true);


        }

    }


    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemSpawnEvent(ItemSpawnEvent event) {
        World world = event.getEntity().getWorld();
        String[] Dissabled = plugin.getConfig().getString("DisabledWorlds").split(",");
        if (Arrays.asList(Dissabled).contains(world.getName())) return;


        Item item = event.getEntity();


        short dura = item.getItemStack().getDurability();
        short Mxdura = item.getItemStack().getType().getMaxDurability();


        if (Mxdura > 1 && dura >= Mxdura)
            item.remove();


        for (Entity e : item.getNearbyEntities(0.5, 0.5, 0.5)) {

            if (e instanceof FallingBlock) {

                FallingBlock FB = (FallingBlock) e;

                if (FB.getMaterial() == item.getItemStack().getType() && getMetadata(FB)) {
                    item.remove();
                    FB.removeMetadata("IsDblock", plugin);
                    break;

                }

            }

        }

        //event.getLocation()


    }


    public boolean getMetadata(FallingBlock FB) {
        List<MetadataValue> values = FB.getMetadata("IsDblock");
        for (MetadataValue value : values) {


            if (value.getOwningPlugin().getDescription().getName().equals(plugin.getDescription().getName())) {
                return (Boolean) value.value();
            }
        }
        return false;
    }


    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.NORMAL)
    public void onProjectileHit(ProjectileHitEvent event) {

        World world = event.getEntity().getWorld();
        String[] Dissabled = plugin.getConfig().getString("DisabledWorlds").split(",");
        if (Arrays.asList(Dissabled).contains(world.getName())) return;


        Entity entity = event.getEntity();

        if (entity.getType() == EntityType.ARROW) {

            if (plugin.getConfig().getBoolean("Arrows2Items") && ((Arrow) entity).getShooter() == null) {

                entity.remove();
                Location loc = event.getEntity().getLocation();
                loc.getWorld().dropItem(loc, new ItemStack(Material.ARROW, 1));

            }

        }
    }


    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.LOW)
    public void onBlockDispenseEvent(BlockDispenseEvent event) {


        World world = event.getBlock().getWorld();
        String[] Dissabled = plugin.getConfig().getString("DisabledWorlds").split(",");
        if (Arrays.asList(Dissabled).contains(world.getName())) return;


        if ((event.getBlock().getType() == Material.DISPENSER) && (event.getBlock().getState() instanceof InventoryHolder)) {

            InventoryHolder IH = (InventoryHolder) event.getBlock().getState();
            Inventory i = IH.getInventory();


            Dispenser disp = (Dispenser) event.getBlock().getState().getData();

            BlockFace face = disp.getFacing();


            if (face == BlockFace.EAST) {


                if (event.getBlock().getData() == 8) face = BlockFace.DOWN;
                if (event.getBlock().getData() == 9) face = BlockFace.UP;
                if (event.getBlock().getData() == 10) face = BlockFace.NORTH;
                if (event.getBlock().getData() == 11) face = BlockFace.SOUTH;
                if (event.getBlock().getData() == 12) face = BlockFace.WEST;
                if (event.getBlock().getData() == 13) face = BlockFace.EAST;


            }


            int range = plugin.getConfig().getInt("DispenserRange");
            if (range > 3)
                range = 3;

            if (range < 0)
                range = 0;


            Block target = event.getBlock().getRelative(face);
            Block target2 = event.getBlock().getRelative(face, 3);
            Block target3 = event.getBlock().getRelative(face, 3 + range);

            ItemStack item = event.getItem();


            if (plugin.getConfig().getBoolean("DispenserUsesTools")) {


                if (this.useableTool(item)) {
                    event.setCancelled(true);

                    Block block2break = null;


                    if (this.ToolonThis(item, target)) {
                        block2break = target;

                    }

                    if (block2break == null) {
                        for (int x = -1; x <= 1; x++) {
                            if (block2break != null) break;
                            for (int y = -1; y <= 1; y++) {
                                if (block2break != null) break;
                                for (int z = -1; z <= 1; z++) {

                                    Block temp = target2.getRelative(x, y, z);

                                    if (this.ToolonThis(item, temp)) {
                                        block2break = temp;
                                        break;
                                    }


                                    temp = target3.getRelative(x, y, z);

                                    if (this.ToolonThis(item, temp)) {
                                        block2break = temp;
                                        break;
                                    }

                                }
                            }
                        }
                    }


                    if (block2break != null) {

                        if (tools.Remove1Durability(item, i))
                            this.UseToolonBlock(item, block2break);
                        else
                            event.setCancelled(false);

                    }
                }//end of tools


                if (this.UseableOnAnimals(item, target)) {


                    event.setCancelled(true);
                    Entity entity = null;


                    Location locD = event.getBlock().getLocation();
                    locD.add(.5, 0, .5);
                    Entity ent = world.dropItem(locD, item);

                    if (entity == null) {
                        Location loc = target2.getLocation();
                        loc.add(.5, 0, .5);
                        ent.teleport(loc);
                        for (Entity e : ent.getNearbyEntities(1.35, 1, 1.35)) {
                            if (this.ToolonEntity(item, e)) {
                                entity = e;
                                break;
                            }
                        }

                    }

                    if (entity == null) {

                        Location loc = target3.getLocation();
                        loc.add(.5, 0, .5);
                        ent.teleport(loc);
                        for (Entity e : ent.getNearbyEntities(1.35, 1, 1.35)) {
                            if (this.ToolonEntity(item, e)) {


                                entity = e;
                                break;
                            }
                        }

                    }
                    ent.remove();


                    if (entity != null) {
                        if (tools.Remove1Durability(item, i)) {

                            this.UseToolonEntity(item, entity);
                        } else {
                            event.setCancelled(false);
                        }

                    }
                } //useable on animal


            }//despences tools


            if (plugin.getConfig().getBoolean("AllowDispensingBlocks")) {
                if (item.getType().isBlock() && item.getType().isSolid()) {
                    FallingBlock FB = target.getWorld().spawnFallingBlock(target.getLocation(), item.getType(), item.getData().getData());
                    FB.setMetadata("IsDblock", new FixedMetadataValue(plugin, true));

                }
            } // AllowDispensingBlocks


        }


    }


    private boolean useableTool(ItemStack item) {


        if (item.getType().toString().contains("_AXE")) return true;
        if (item.getType().toString().contains("_PICKAXE")) return true;
        if (item.getType().toString().contains("_SPADE")) return true;
        if (item.getType().toString().contains("_HOE")) return true;

        if (item.getType() == Material.FISHING_ROD) return true;

        return false;
    }


    private boolean ToolonThis(ItemStack item, Block target) {


        if (item.getType().toString().contains("_AXE")) {
            if (!tools.canIbuild(target.getLocation(), plugin)) return false;
            if (target.getType() == Material.LOG ||
                    target.getType() == Material.LEAVES ||
                    target.getType() == Material.HUGE_MUSHROOM_1 ||
                    target.getType() == Material.HUGE_MUSHROOM_2)
                return true;
        }

        if (item.getType().toString().contains("_SPADE")) {
            if (!tools.canIbuild(target.getLocation(), plugin)) return false;
            if (target.getType() == Material.CLAY ||
                    target.getType() == Material.DIRT ||
                    target.getType() == Material.GRASS ||
                    target.getType() == Material.GRAVEL ||
                    target.getType() == Material.MYCEL ||
                    target.getType() == Material.SOUL_SAND ||
                    target.getType() == Material.SOIL ||
                    target.getType() == Material.SNOW ||
                    target.getType() == Material.SNOW_BLOCK ||
                    target.getType() == Material.SAND)
                return true;
        }


        if (item.getType().toString().contains("_HOE")) {
            if (!tools.canIbuild(target.getLocation(), plugin)) return false;
            if (target.getType() == Material.DIRT ||
                    target.getType() == Material.GRASS)
                return true;
        }


        if (item.getType() == Material.FISHING_ROD)
            if (target.getType() == Material.STATIONARY_WATER || target.getType() == Material.WATER)
                return true;


        if (item.getType().toString().contains("_PICKAXE")) {
            if (!tools.canIbuild(target.getLocation(), plugin)) return false;
            if (target.getType() == Material.STONE ||
                    target.getType() == Material.COBBLESTONE ||
                    target.getType() == Material.SANDSTONE ||
                    target.getType().toString().contains("_ORE"))
                return true;


        }


        return false;
    }


    private void UseToolonBlock(ItemStack item, Block target) {


        Location loc = target.getLocation();
        loc.add(.5, .5, .5);

        if (item.getType().toString().contains("_SPADE") &&
                target.getType() == Material.SNOW) {
            target.getWorld().dropItem(loc, new ItemStack(Material.SNOW_BALL, 1));

        }


        if (item.getType() == Material.FISHING_ROD) {
            int fish = Smart_Items.random.nextInt(100);
            short x = 2;

            if (fish > 2) x = 3;
            if (fish > 13) x = 1;
            if (fish > 25) x = 0;


            target.getWorld().dropItem(loc, new ItemStack(Material.RAW_FISH, 1, x));
            return;
        }


        if (item.getType().toString().contains("_HOE") && (target.getType() == Material.DIRT || target.getType() == Material.GRASS)) {


            target.setType(Material.SOIL);
            return;
        }


        target.breakNaturally(item);
    }


    private boolean UseableOnAnimals(ItemStack item, Block target) {


        if (item.getType() == Material.SHEARS) return true;
        if (item.getType().toString().contains("_SWORD")) return true;

        return false;
    }


    private boolean ToolonEntity(ItemStack item, Entity e) {


        if (item.getType() == Material.SHEARS) {
            if (e instanceof MushroomCow || e instanceof Sheep) {
                boolean doit = true;

                Ageable ea = (Ageable) e;
                if (!ea.isAdult()) doit = false;


                if (e instanceof Sheep) {

                    Sheep se = (Sheep) e;
                    if (se.isSheared()) doit = false;
                }

                return doit;

            }
        }


        if (item.getType().toString().contains("_SWORD")) {
            if (e instanceof LivingEntity) return true;
        }


        return false;
    }


    private void UseToolonEntity(ItemStack item, Entity e) {


        World world = e.getWorld();
        if (e instanceof Sheep && item.getType() == Material.SHEARS) {

            Sheep Asheep = (Sheep) e;
            int amount = Smart_Items.random.nextInt(3) + 1;

            Wool wool = new Wool(Asheep.getColor());
            ItemStack stack = wool.toItemStack(amount);

            world.dropItemNaturally(Asheep.getLocation(), stack);
            Asheep.setSheared(true);

        }


        if (e instanceof MushroomCow && item.getType() == Material.SHEARS) {

            world.dropItemNaturally(e.getLocation(), new ItemStack(Material.RED_MUSHROOM, 1));
            world.dropItemNaturally(e.getLocation(), new ItemStack(Material.RED_MUSHROOM, 1));
            world.dropItemNaturally(e.getLocation(), new ItemStack(Material.RED_MUSHROOM, 1));
            world.dropItemNaturally(e.getLocation(), new ItemStack(Material.RED_MUSHROOM, 1));
            world.dropItemNaturally(e.getLocation(), new ItemStack(Material.RED_MUSHROOM, 1));


            world.spawnEntity(e.getLocation(), EntityType.COW);
            e.remove();


        }


        if (item.getType().toString().contains("_SWORD")) {
            if (e instanceof LivingEntity) {
                LivingEntity le = (LivingEntity) e;

                le.damage(4.0);

            }


        }


    }


    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerPickupItemEvent(PlayerPickupItemEvent event) {
        World world = event.getItem().getWorld();
        String[] Dissabled = plugin.getConfig().getString("DisabledWorlds").split(",");
        if (Arrays.asList(Dissabled).contains(world.getName())) return;


        if (event.getItem().getItemStack().getType() == Material.FIRE)
            event.setCancelled(true);


    }


}












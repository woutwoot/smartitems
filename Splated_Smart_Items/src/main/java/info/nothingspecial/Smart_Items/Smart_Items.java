package info.nothingspecial.Smart_Items;


import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;



/* Smart_Items
 * plant trees on grass/dirt
 * plant crops on farmland
 * wart on soul sand
 * bucket near cow makes milk
 * Powered CAULDRON picks up items puts in chests
 * food near animals breeds them
 * Item Frames filter CAULDRON picks ups
 * steps to prevent too many items.
 */


public class Smart_Items extends JavaPlugin implements Runnable {


    public static Random random = new Random();
    public static boolean Debug = false;
    public final String TrashName = ChatColor.RESET + "Trash";
    int CropPlantingTimer;
    int TreePlantingTimer;
    boolean fullStack = true;

    public static void info(String msg) {
        if (Debug) {
            ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();

            if (console == null)
                Bukkit.getLogger().info(msg);
            else
                console.sendMessage(ChatColor.GOLD + msg);


        }
    }


    @Override
    public void onEnable() {


        if (!new File(getDataFolder(), "config.yml").exists()) {
            this.getLogger().info("Config Missing Making New one");
            this.getConfig().set("CropPlantingTimer", 120);
            this.getConfig().set("TreePlantingTimer", 120);
            this.getConfig().set("UseWorldGuard", true);


            this.getConfig().set("AllowAutoCraft", true);
            this.getConfig().set("AllowBreeding", true);
            this.getConfig().set("AllowCauldronLift", true);

            this.getConfig().set("Arrows2Items", true);

            this.getConfig().set("DispenserUsesTools", true);

            this.getConfig().set("AllowDispensingBlocks", true);
            this.getConfig().set("DispenserRange", 1);


            this.getConfig().set("DisabledWorlds", "world1_nether,world1");
            this.saveConfig();
        }


        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("server.properties"));
            Debug = "true".equals(prop.getProperty("debug"));
        } catch (IOException ex) {
        }
        info(this.getName() + " DEBUG ON");


        CropPlantingTimer = this.getConfig().getInt("CropPlantingTimer");
        TreePlantingTimer = this.getConfig().getInt("TreePlantingTimer");

        getServer().getScheduler().scheduleSyncRepeatingTask(this, this, 10, 5);
        new smart_dispenser(this);


    }

    @Override
    public void onDisable() {

    }


    @SuppressWarnings("deprecation")
    @Override
    public void run() {

        String[] Dissabled = this.getConfig().getString("DisabledWorlds").split(",");


        for (World world : getServer().getWorlds()) {


            if (Arrays.asList(Dissabled).contains(world.getName())) continue;


            Collection<Item> itemlist = world.getEntitiesByClass(Item.class);


            for (Item item : itemlist) {


                if (itemlist.size() > 1000) {
                    Smart_Items.info("Item count too hight! " + itemlist.size());

                    item.remove();
                    return;

                }

                if (item.isDead()) continue;


                if (itemlist.size() > 950) {
                    if (item.getTicksLived() > 3000 && item.getVelocity().length() < 0.001) {
                        item.remove();
                        continue;
                    }

                    if (item.getTicksLived() > 4000) {
                        item.remove();
                        continue;
                    }


                }


                Block blockBelow = item.getLocation().getBlock().getRelative(0, -1, 0);
                Block blockAbove = item.getLocation().getBlock().getRelative(0, 1, 0);
                Block blockin = item.getLocation().getBlock();


                //if(item.getItemStack().getType() == Material.CLAY_BALL && blockBelow.getType() == Material.AIR ){
                //&& item.getVelocity().lengthSquared() < 1


                if (blockAbove.getType() == Material.CAULDRON && this.getConfig().getBoolean("AllowCauldronLift")) {

                    if (blockAbove.isBlockPowered()) {
                        if (tools.PickItUp(blockAbove, item)) {


                            Block topblock = blockAbove;
                            boolean go = true;
                            int y = 0;
                            while (go) {
                                if (topblock.getType() == Material.CAULDRON) {
                                    topblock = topblock.getRelative(0, 1, 0);
                                    y = y + 1;
                                } else {
                                    go = false;
                                }


                            }

                            ItemStack item2 = item.getItemStack().clone();
                            if (!fullStack) item2.setAmount(1);


                            //info( topblock.getType() + " top of c stack " + (topblock.getState() instanceof InventoryHolder) );

                            if (topblock.getState() instanceof InventoryHolder && (
                                    topblock.getType() == Material.TRAPPED_CHEST ||
                                            topblock.getType() == Material.DROPPER ||
                                            topblock.getType() == Material.DISPENSER ||
                                            topblock.getType() == Material.CHEST)) {


                                Inventory inventory = ((InventoryHolder) (topblock.getState())).getInventory();
                                HashMap<Integer, ItemStack> leftover = inventory.addItem(item2);

                                if (leftover.isEmpty()) {
                                    if (fullStack) {
                                        item.remove();
                                    } else {
                                        tools.remove1(item);
                                    }

                                } else {
                                    item.setItemStack(leftover.get(0));
                                }


                            } else {

                                Location location = blockAbove.getLocation().add(.5, 0 + y, .5);
                                if (fullStack) {
                                    item.remove();
                                } else {
                                    tools.remove1(item);
                                }
                                Item thing = world.dropItem(location, item2);
                                thing.setTicksLived(item.getTicksLived() + 1);

                            }
                        }


                    }
                } //AllowCauldronLift


                if (blockBelow.getType() == Material.WORKBENCH && this.getConfig().getBoolean("AllowAutoCraft") && blockBelow.isBlockIndirectlyPowered()) {


                    List<ItemFrame> frames = tools.GetFrame(blockBelow);

                    for (ItemFrame frame : frames) {
                        //if (frame != null){

                        List<Item> craftStack = new ArrayList<Item>();
                        Block testblock = item.getLocation().getBlock();
                        craftStack.add(item);

                        List<Entity> nearItem = item.getNearbyEntities(1, 0, 1);
                        for (Entity i : nearItem) {
                            if (i instanceof Item) {
                                Item it = (Item) i;
                                if (testblock.equals(it.getLocation().getBlock()) && (!it.isDead())) {
                                    craftStack.add(it);
                                }
                            }
                        }


                        List<Recipe> a = this.getServer().getRecipesFor(frame.getItem());
                        //Smart_Items.info( "frame.getItem().getType() " + frame.getItem().getType());
                        if (frame.getItem().getType() == Material.FIREWORK_CHARGE) {


                            ItemStack made = AutoCraft.Makeit(null, craftStack, frame.getItem());
                            if (made != null) item.getWorld().dropItem(item.getLocation(), made);

                        }

                        ItemStack made = null;
                        if (frame.getItem().getType() == Material.POTION &&
                                frame.getItem().getData().getData() == 0 &&
                                blockin.getType() == Material.STATIONARY_WATER) {

                            //Smart_Items.info( "POTION");
                            made = AutoCraft.Makeit(null, craftStack, frame.getItem());
                            if (made != null) item.getWorld().dropItem(item.getLocation(), made);
                        }


                        for (Recipe resp : a) {


                            made = AutoCraft.Makeit(resp, craftStack, frame.getItem());
                            if (made != null) item.getWorld().dropItem(item.getLocation(), made);
                        }

                        if (made != null) break;


                    }


                } // workbench


                if (item.getItemStack().getType() == Material.BUCKET) {


                    Cow cow = null;
                    for (Entity entity : item.getNearbyEntities(1, 1, 1)) {
                        if (entity instanceof Cow) {
                            cow = (Cow) entity;
                            if (cow.isAdult()) break;
                            cow = null;
                        }
                    }

                    if (cow != null) {
                        tools.remove1(item);
                        world.dropItem(item.getLocation(), new ItemStack(Material.MILK_BUCKET, 1));
                    }

                }


                if (item.getItemStack().getType() == Material.BOWL) {
                    MushroomCow cow = null;
                    for (Entity entity : item.getNearbyEntities(1, 1, 1)) {
                        if (entity instanceof Cow) {
                            cow = (MushroomCow) entity;
                            if (cow.isAdult()) break;
                            cow = null;
                        }
                    }

                    if (cow != null) {
                        tools.remove1(item);
                        world.dropItem(item.getLocation(), new ItemStack(Material.MUSHROOM_SOUP, 1));
                    }

                }


                if (item.getTicksLived() > CropPlantingTimer && CropPlantingTimer > 0) {
                    //item.remove();

                    this.Breed(item);


                    if (blockBelow.getType() == Material.SOUL_SAND || blockBelow.getType() == Material.SOIL) {

                        //if (tools.canIbuild(item.getLocation()) ){
                        int size = 3;


                        Loop:
                        {

                            int x = 0, z = 0, dx = 0, dz = -1;
                            int t = size;
                            int maxI = t * t;

                            for (int i = 0; i < maxI; i++) {

                                if ((-size < x && x <= size) && (-size < z && z <= size)) {


                                    Block TmpblockBelow = blockBelow.getRelative(x, 0, z);
                                    Block Tmpblockin = blockin.getRelative(x, 0, z);

                                    // Netherwarts
                                    if ((TmpblockBelow.getType() == Material.SOUL_SAND) &&
                                            (Tmpblockin.getType() == Material.AIR) &&
                                            (item.getItemStack().getType().getId() == 372)) { //netherwart id
                                        Tmpblockin.setType(Material.NETHER_WARTS);
                                        tools.remove1(item);
                                        break Loop;
                                    }


                                    // Crops
                                    if (TmpblockBelow.getType() == Material.SOIL &&
                                            Tmpblockin.getType() == Material.AIR) {


                                        if (item.getItemStack().getType() == Material.SEEDS) {
                                            Tmpblockin.setType(Material.CROPS);
                                            tools.remove1(item);
                                            break Loop;
                                        }

                                        if (item.getItemStack().getType() == Material.MELON_SEEDS) {
                                            Tmpblockin.setType(Material.MELON_STEM);
                                            tools.remove1(item);
                                            break Loop;
                                        }

                                        if (item.getItemStack().getType() == Material.PUMPKIN_SEEDS) {
                                            Tmpblockin.setType(Material.PUMPKIN_STEM);
                                            tools.remove1(item);
                                            break Loop;
                                        }


                                        if (item.getItemStack().getType() == Material.CARROT_ITEM) {
                                            Tmpblockin.setType(Material.CARROT);
                                            tools.remove1(item);
                                            break Loop;
                                        }

                                        if (item.getItemStack().getType() == Material.POTATO_ITEM) {
                                            Tmpblockin.setType(Material.POTATO);
                                            tools.remove1(item);
                                            break Loop;
                                        }


                                    }


                                }

                                if ((x == z) || ((x < 0) && (x == -z)) || ((x > 0) && (x == 1 - z))) {
                                    t = dx;
                                    dx = -dz;
                                    dz = t;
                                }
                                x += dx;
                                z += dz;
                            }
                        }//loop

                    }

                    if (item.getItemStack().getType() == Material.SUGAR_CANE &&
                            blockin.getType() == Material.AIR &&

                            (blockBelow.getType() == Material.DIRT || blockBelow.getType() == Material.SAND || blockBelow.getType() == Material.GRASS)) {

                        if (blockin.getRelative(BlockFace.NORTH).getType() == Material.STATIONARY_WATER ||
                                blockBelow.getRelative(BlockFace.SOUTH).getType() == Material.STATIONARY_WATER ||
                                blockBelow.getRelative(BlockFace.EAST).getType() == Material.STATIONARY_WATER ||
                                blockBelow.getRelative(BlockFace.WEST).getType() == Material.STATIONARY_WATER ||
                                blockBelow.getRelative(BlockFace.NORTH).getType() == Material.WATER ||
                                blockBelow.getRelative(BlockFace.SOUTH).getType() == Material.WATER ||
                                blockBelow.getRelative(BlockFace.EAST).getType() == Material.WATER ||
                                blockBelow.getRelative(BlockFace.WEST).getType() == Material.WATER) {

                            blockin.setType(Material.SUGAR_CANE_BLOCK);
                            tools.remove1(item);

                        }
                    }


                }


                if (item.getTicksLived() > TreePlantingTimer && TreePlantingTimer > 0) {

                    // tree planting
                    if ((item.getItemStack().getType() == Material.SAPLING) &&
                            ((blockBelow.getType() == Material.DIRT) || blockBelow.getType() == Material.GRASS) &&
                            (blockin.getType() == Material.AIR)) {


                        if (tools.canIbuild(item.getLocation(), this)) {


                            blockin.setType(item.getItemStack().getType());

                            blockin.setData(item.getItemStack().getData().getData());

                            //blockin.setTypeIdAndData(item.getItemStack().getType().getId(), b, false);
                            tools.remove1(item);
                        }


                    }


                    // mushroom planting
                    if ((item.getItemStack().getType() == Material.RED_MUSHROOM) &&
                            ((blockBelow.getType() == Material.DIRT) || blockBelow.getType() == Material.MYCEL) &&
                            (blockin.getType() == Material.AIR)) {
                        if (tools.canIbuild(item.getLocation(), this)) {
                            blockin.setType(Material.RED_MUSHROOM);
                            tools.remove1(item);
                        }
                    }

                    // mushroom planting
                    if ((item.getItemStack().getType() == Material.BROWN_MUSHROOM) &&
                            ((blockBelow.getType() == Material.DIRT) || blockBelow.getType() == Material.MYCEL) &&
                            (blockin.getType() == Material.AIR)) {
                        if (tools.canIbuild(item.getLocation(), this)) {
                            blockin.setType(Material.BROWN_MUSHROOM);
                            tools.remove1(item);
                        }
                    }

                }


                if (item.getItemStack().getType() == Material.INK_SACK && item.getItemStack().getData().getData() == 15) {

                    if (tools.useBoneMealon(blockin, this))
                        tools.remove1(item);

                }


            }
        }
    }


    private void Breed(Item item) {

        if (!this.getConfig().getBoolean("AllowBreeding")) return;
        if (item.getItemStack().getAmount() < 2) return;
        /*
        The specific items for each breedable animal are as follows

		Horses: Golden Apples or Golden Carrots to breed.
		Wheat, Apples, Sugar, Bread, Golden Apples and Hay Bales to feed for growth and healing.

		Sheep, Cows and Mooshrooms: Wheat.

		Pigs: Carrots (on the Xbox version, Pigs are led and bred with wheat) (With Pocket Edition pigs can be bred with a Potato and Beetroot)

		Chickens: Seeds. Melon seeds, pumpkin seeds

		Tamed Wolves: any type of meat, whether cooked, raw, or rotten.


		Tamed Cats: raw fish.
		 */


        Material IT = item.getItemStack().getType();
        List<EntityType> anminalList = new ArrayList<EntityType>();

        if (IT == Material.GOLDEN_APPLE || IT == Material.GOLDEN_CARROT) {
            anminalList.add(EntityType.HORSE);
        }


        if (IT == Material.WHEAT) {


            anminalList.add(EntityType.MUSHROOM_COW);
            anminalList.add(EntityType.COW);
            anminalList.add(EntityType.SHEEP);
        }


        if (IT == Material.RAW_FISH)
            anminalList.add(EntityType.OCELOT);

        if (IT == Material.CARROT)
            anminalList.add(EntityType.PIG);


        if (IT == Material.SEEDS ||
                IT == Material.MELON_SEEDS ||
                IT == Material.PUMPKIN_SEEDS) {
            anminalList.add(EntityType.CHICKEN);
        }


        //anminalList.put(EntityType.WOLF, 0);


        if (anminalList.size() == 0) return;


        Ageable mom = null;
        Ageable dad = null;

        for (EntityType ET : anminalList) {
            mom = null;
            dad = null;
            //info("breed "+ET + " with " + IT);
            for (Entity entity : item.getNearbyEntities(1, 1, 1)) {
                if (entity instanceof Ageable) {
                    Ageable critter = (Ageable) entity;
                    if (critter.getType() == ET && critter.canBreed()) {
                        if (dad == null) {
                            dad = critter;
                            continue;
                        }
                        if (mom == null) {
                            mom = critter;
                            break;
                        }
                    }
                }
            }

            if (mom != null && dad != null) break;
        }


        if (mom != null && dad != null) {
            mom.setBreed(false);
            dad.setBreed(false);

            Ageable baby = (Ageable) mom.getWorld().spawnEntity(mom.getLocation().add(0, 1, 0), mom.getType());
            baby.setBaby();
            if (baby instanceof Sheep && dad instanceof Sheep)
                ((Sheep) baby).setColor(((Sheep) dad).getColor());


            tools.removeX(item, 2);
        }


    }


	/*



						for(Entity entity2: critter.getNearbyEntities(2, 2, 2)){
							if(entity2 instanceof Ageable){
								Ageable critter2 = (Ageable) entity2;

								if ((critter2.canBreed()) && 
										(critter != critter2 ) &&
										(critter.getClass() == critter2.getClass()) ){



									critter.setBreed(false);
									critter2.setBreed(false);

									Ageable critter3 = (Ageable) critter2.getWorld().spawnEntity(critter2.getLocation().add(0, 1, 0), critter2.getType());
									critter3.setBaby();


									tools.removeX(item, 2);
									broke = true;
									if (broke) break;
								}



							}
						}

				}
			}
		}




	 */


}

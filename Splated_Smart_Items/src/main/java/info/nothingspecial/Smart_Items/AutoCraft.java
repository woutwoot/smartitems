package info.nothingspecial.Smart_Items;

import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.material.Dye;

import java.util.*;
import java.util.Map.Entry;


/* AutoCraft
 * Create items from recipes
 * POTION filled with water
 * FIREWORK and FIREWORK_CHARGE get parts needed.
 * Creeper head broken
 * Makes Water Potions if water is above 
 *
 *
 *
 */


public class AutoCraft {


    @SuppressWarnings("deprecation")
    public static ItemStack Makeit(Recipe recipe, List<Item> craftStack, ItemStack Tobuild) {
        List<ItemStack> re = new ArrayList<ItemStack>();


        if (recipe != null) {
            re = AutoCraft.WhatDoINeedToCraft(recipe);

        }


        if (Tobuild.getType() == Material.POTION) {
            re.add(new ItemStack(Material.GLASS_BOTTLE, 1));
        }


        if (Tobuild.getType() == Material.FIREWORK_CHARGE) {
            re.add(new ItemStack(Material.SULPHUR, 1));
            if (Tobuild.hasItemMeta()) {

                FireworkEffectMeta FEM = (FireworkEffectMeta) Tobuild.getItemMeta();
                Type type = FEM.getEffect().getType();


                if (type == Type.CREEPER) {
                    ItemStack head = new ItemStack(Material.SKULL_ITEM, 1);
                    re.add(head);

                }
                if (type == Type.STAR)
                    re.add(new ItemStack(Material.GOLD_NUGGET, 1));

                if (type == Type.BURST)
                    re.add(new ItemStack(Material.FEATHER, 1));

                if (type == Type.BALL_LARGE)
                    re.add(new ItemStack(Material.FIREBALL, 1));

                if (FEM.getEffect().hasFlicker())
                    re.add(new ItemStack(Material.GLOWSTONE_DUST, 1));

                if (FEM.getEffect().hasTrail())
                    re.add(new ItemStack(Material.DIAMOND, 1));


                List<Color> color1 = FEM.getEffect().getColors();
                List<Color> color2 = FEM.getEffect().getFadeColors();


                HashMap<DyeColor, Integer> colors = new HashMap<DyeColor, Integer>();


                for (int c = 0; c < color1.size(); c++) {
                    DyeColor dc = DyeColor.getByFireworkColor(color1.get(c));

                    Integer count = colors.get(dc);
                    if (count == null) {
                        colors.put(dc, 1);
                    } else {
                        count++;
                        colors.put(dc, count);
                    }
                }
                for (int c = 0; c < color2.size(); c++) {
                    DyeColor dc = DyeColor.getByFireworkColor(color2.get(c));

                    Integer count = colors.get(dc);
                    if (count == null) {
                        colors.put(dc, 1);
                    } else {
                        count++;
                        colors.put(dc, count);
                    }
                }

                //HashMap<DyeColor, Integer>()

                for (Entry<DyeColor, Integer> entry : colors.entrySet()) {
                    DyeColor dc = entry.getKey();
                    Integer value = entry.getValue();
                    ItemStack is = new ItemStack(Material.INK_SACK, value);
                    Dye dye = new Dye();
                    dye.setColor(dc);
                    is.setData(dye);
                    re.add(is);
                }


            } else {
                return null;
            }

        }


        if (Tobuild.getType() == Material.FIREWORK) {

            recipe = null;
            re.clear();
            re.add(new ItemStack(Material.PAPER, 1));
            if (Tobuild.hasItemMeta()) {

                FireworkMeta FM = (FireworkMeta) Tobuild.getItemMeta();
                re.add(new ItemStack(Material.SULPHUR, FM.getPower()));


                ItemStack charge = new ItemStack(Material.FIREWORK_CHARGE, 1);


                if (FM.getEffects().size() == 1) {
                    FireworkEffect fwe = FM.getEffects().get(0);


                    FireworkEffectMeta FEM = (FireworkEffectMeta) charge.getItemMeta();
                    FEM.setEffect(fwe);

                    charge.setItemMeta(FEM);
                } else {
                    Smart_Items.info("FireworkEffect is more then 1!");
                    for (FireworkEffect s : FM.getEffects()) {
                        Smart_Items.info("FireworkEffect " + s.getType());
                    }

                }


                re.add(charge);


            } else {
                re.add(new ItemStack(Material.SULPHUR, 1));
            }


        }
        // end get pattern


        boolean[] tests = new boolean[re.size()];


        if (!re.isEmpty()) {


            for (int x = 0; x < re.size(); x++) {
                ItemStack recItem = re.get(x);


                boolean Have = false;

                for (Item nearItem1 : craftStack) {
                    ItemStack nearitemstack = nearItem1.getItemStack();


                    if (recItem.getType() == nearitemstack.getType()) {


                        if (recItem.getAmount() <= nearitemstack.getAmount()) {
                            byte dat = recItem.getData().getData();

                            //if (recItem.getType() == Material.SKULL_ITEM )dat = -1;


                            if (dat == -1 || (dat == nearitemstack.getData().getData())) {


                                if (recItem.hasItemMeta() && nearitemstack.hasItemMeta()) {
                                    //Smart_Items.info("recItem.getItemMeta() "+ 	recItem.getItemMeta());
                                    //Smart_Items.info("nearitemstack.getItemMeta() "+ 	nearitemstack.getItemMeta());

                                    if (recItem.getItemMeta().equals(nearitemstack.getItemMeta())) {

                                        Have = true;
                                    }
                                } else {

                                    Have = true;
                                }

                            }

                        }

                    }
                }

                tests[x] = Have;

                //Smart_Items.info(Have+" " +recItem.getAmount()+" " + recItem.getType() + ":" +recItem.getData());

            }


            for (int x = 0; x < re.size(); x++) {

                if (!tests[x]) {
                    return null;

                }
            }


            for (int x = 0; x < re.size(); x++) {
                ItemStack recItem = re.get(x);
                for (Item item : craftStack) {


                    if (recItem.getType() == item.getItemStack().getType()) {

                        if (recItem.getAmount() <= item.getItemStack().getAmount()) {
                            //Smart_Items.info( recItem.getType() +  " Amount ok");
                            byte dat = recItem.getData().getData();

                            //if (recItem.getType() == Material.SKULL_ITEM )dat = -1;


                            if (dat == -1 || (dat == item.getItemStack().getData().getData())) {


                                tools.removeX(item, recItem.getAmount());

                            }

                        }
                    }
                }

            }

            if (recipe != null) return recipe.getResult();
            return Tobuild;

        }


        return null;
    }


    public static List<ItemStack> WhatDoINeedToCraft(Recipe resp) {
        List<ItemStack> craftStack = new ArrayList<ItemStack>();

        if (resp instanceof ShapedRecipe) {
            ShapedRecipe Sresp = (ShapedRecipe) resp;

            Map<Character, ItemStack> d = Sresp.getIngredientMap();


            Iterator<Entry<Character, ItemStack>> it = d.entrySet().iterator();
            while (it.hasNext()) {
                Entry<Character, ItemStack> pairs = it.next();


                if (pairs.getValue() != null)
                    craftStack.add(pairs.getValue());
            }

        }


        if (resp instanceof ShapelessRecipe) {

            ShapelessRecipe SLresp = (ShapelessRecipe) resp;
            List<ItemStack> b = SLresp.getIngredientList();
            for (ItemStack temp : b) {
                craftStack.add(temp);
            }
        }

        List<ItemStack> craftStack2 = new ArrayList<ItemStack>();

        for (ItemStack it : craftStack) {

            if (!craftStack2.contains(it))
                craftStack2.add(it);

        }

        for (ItemStack it : craftStack2) {
            int count = 0;

            for (ItemStack it2 : craftStack) {
                if (it.equals(it2))
                    count++;
            }

            it.setAmount(count);
        }

        return craftStack2;
    }


}

package info.nothingspecial.Smart_Items;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.ItemFrame;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Tree;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;


public class tools {


    public static TreeType getTree(Block block) {

        TreeType tretyp = null;


        if (block.getState().getData() instanceof Tree) {
            Tree temptree = (Tree) block.getState().getData();

            TreeSpecies ts = temptree.getSpecies();
            if (ts != TreeSpecies.GENERIC)
                tretyp = TreeType.valueOf(ts.toString());


        }


        if (tretyp == null) {
            @SuppressWarnings("deprecation")
            byte data = block.getData();


            switch (data) {
                case 4:
                    tretyp = TreeType.ACACIA;
                    break;

                default:

                    Smart_Items.info("Unknow Tree " + data + " data" + block.getState().getData());
                    tretyp = TreeType.TREE;
            }
        }


        if (tretyp == null) tretyp = TreeType.TREE;

        Smart_Items.info("getTree = " + tretyp);
        return tretyp;

    }


    public static void remove1(Item item) {
        tools.removeX(item, 1);
    }


    public static void removeX(Item item, int x) {

        if (item.getItemStack().getAmount() > x) {
            item.getItemStack().setAmount(item.getItemStack().getAmount() - x);
        } else {
            item.remove();
        }
    }


    @SuppressWarnings("deprecation")
    public static boolean PickItUp(Block block, Item item) {
        List<ItemFrame> frames = tools.GetFrame(block);

        if (frames.size() == 0) return true;

        for (ItemFrame frame : frames) {


            if (frame.getItem() != null) {

                if (item.getItemStack().getType() == frame.getItem().getType()
                        && item.getItemStack().getData().getData() == frame.getItem().getData().getData()) {

                    return true;
                }
            }

        }
        return false;
    }


    public static List<ItemFrame> GetFrame(Block block) {
        List<ItemFrame> frames = new ArrayList<ItemFrame>();

        World world = block.getWorld();
        Location loc = block.getLocation().add(.5, .5, .5);
        Entity tempE = world.spawnEntity(loc, EntityType.EXPERIENCE_ORB);


        List<Entity> near = tempE.getNearbyEntities(1, 1, 1);
        for (Entity e : near) {
            if (e instanceof ItemFrame) {
                ItemFrame frametmp = (ItemFrame) e;


                Block blockt = world.getBlockAt(frametmp.getLocation()).getRelative(frametmp.getAttachedFace());


                if (blockt.equals(block)) {
                    frames.add(frametmp);
                }
            }
        }


        tempE.remove();

        return frames;
    }


    public static boolean Remove1Durability(ItemStack item, Inventory i) {


        for (ItemStack inven : i.getContents()) {

            if (inven != null)
                if (inven.getType().equals(item.getType())) {
                    inven.setDurability((short) (inven.getDurability() + 1));
                    break;
                }

        }

        int nullcount = 0;
        boolean clear = false;
        for (int x = 0; x < i.getContents().length; x++) {

            ItemStack is = i.getItem(x);
            if (is != null) {

                if (is.getDurability() > is.getType().getMaxDurability() && is.getType().getMaxDurability() > 1) {
                    clear = true;
                    i.clear(x);

                }
            } else {
                nullcount++;
            }
        }


        if (nullcount == i.getContents().length - 1 && clear)
            return false;

        return true;


    }


    @SuppressWarnings("deprecation")
    public static boolean useBoneMealon(Block blockin, Smart_Items SI) {


        if (blockin == null) return false;
        boolean worked = false;

        int amount = Smart_Items.random.nextInt(4) + 1;


        if ((blockin.getType() == Material.CROPS) ||
                (blockin.getType() == Material.CARROT) ||
                (blockin.getType() == Material.POTATO) ||
                (blockin.getType() == Material.MELON_STEM) ||
                (blockin.getType() == Material.PUMPKIN_STEM)) {


            byte growth = blockin.getData();

            if (growth < 7) {
                growth = (byte) (growth + amount);

                if (growth > 7) growth = (byte) 7;
                blockin.setData(growth);

                worked = true;
            }
        }


        if (tools.canIbuild(blockin.getLocation(), SI)) {

            if (blockin.getType() == Material.SAPLING) {
                TreeType treetype = tools.getTree(blockin);
                Material tmpblock = blockin.getType();
                blockin.setType(Material.AIR); // temp make it air
                if (blockin.getWorld().generateTree(blockin.getLocation(), treetype)) {
                    worked = true;
                } else
                    blockin.setType(tmpblock);
            }

            if (blockin.getType() == Material.RED_MUSHROOM) {
                byte b = blockin.getData();
                blockin.setTypeId(0); // temp make it air

                if (blockin.getWorld().generateTree(blockin.getLocation(), TreeType.RED_MUSHROOM)) {
                    worked = true;
                } else
                    blockin.setTypeIdAndData(Material.RED_MUSHROOM.getId(), b, false);
            }

            if (blockin.getType() == Material.BROWN_MUSHROOM) {
                byte b = blockin.getData();
                blockin.setTypeId(0); // temp make it air
                if (blockin.getWorld().generateTree(blockin.getLocation(), TreeType.BROWN_MUSHROOM)) {
                    worked = true;
                } else
                    blockin.setTypeIdAndData(Material.BROWN_MUSHROOM.getId(), b, false);
            }
        }


        if (worked) {
            //Effect e = Effect.SMOKE;
            Effect e = Effect.getById(2001);
            blockin.getWorld().playEffect(blockin.getLocation(), e, Material.EMERALD_BLOCK.getId());
            return true;
        }

        return false;
    }


    public static boolean canIbuild(Location location, Smart_Items SI) {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) return true;
        if (!SI.getConfig().getBoolean("UseWorldGuard")) return true;
        WorldGuardPlugin api = (WorldGuardPlugin) plugin;
        if (api.getRegionManager(location.getWorld()).getApplicableRegions(location).size() > 0) return false;
        return true;
    }


}
